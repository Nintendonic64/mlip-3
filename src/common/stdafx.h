/*   MLIP is a software for Machine Learning Interatomic Potentials
 *   MLIP is released under the "New BSD License", see the LICENSE file.
 *   Contributors: Alexander Shapeev, Evgeny Podryabinkin
 */

#pragma once

#include <time.h>
#include <stdio.h>
#include <set>
#include <map>
#include <list>
#include <cmath>
#include <array>
#include <vector>
#include <string>
#include <random>
#include <fstream>
#include <iomanip>
#include <utility>
#include <string.h> //memcpy, memset in it in linux
#include <iostream>

//#define MLIP_MPI
//#define MLIP_DEBUG
